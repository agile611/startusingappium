package com.itnove.pages.amazon;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by guillem on 01/03/16.
 */
public class AmazonProductPage {

    private WebDriver driver;

    @FindBy(id = "add-to-cart-button")
    public WebElement addToCartButton;

    public AmazonProductPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void clickOnAddToCartButton() {
        addToCartButton.click();
    }
}
