# Start Using Appium Examples

This is the mini-sized package to start use Appium with iOS mobile Safari browser and on Android browser

## Pre-requisite:
1. MacOS - Very important, not supporting any other OS.
2. Java
3. Maven
4. Appium 1.6.3
5. Xcode with iPhone Simulator for iOS and safari mobile browser test
6. Android SDK + VirtualBox + Genymotion for Android default mobile browser test

## General Environment installation
1. Install Xcode from the Appstore
2. Install Homebrew from http://www.brew.sh
3. Install Java
```
brew cask install java
```
4. Install Maven
```
brew install maven
```
5. Install nodeJS
```
brew install node
```
6. Install Appium via terminal
```
npm install -g appium
```
7. Install Appium Doctor via terminal
```
npm install -g appium-doctor
```
8. Install Appium GUI - https://bitbucket.org/appium/appium.app/downloads/appium-1.5.3.dmg
9. Install virtualbox
```
brew cask install virtualbox
```

## General IOS installation
1. Install ideviceinstall
```
brew install ideviceinstaller
```
2. Install ios-webkit-debug-proxy
```
brew install ios-webkit-debug-proxy
```
3. Install carthage
```
brew install carthage
```
4. Install ios-deploy
```
npm install -g ios-deploy
```
5. Para las signaturas IOS - https://github.com/DanTheMan827/ios-app-signer/releases
6. Authorize IOS if GUI does not authorize it - https://github.com/appium/authorize-ios
7. Potential issue using lockdown https://github.com/google/ios-webkit-debug-proxy/issues/160

## General Android installation
1. Install Android SDK
```
brew install android-sdk
```
2. Listar todos los SDK - android list sdk --all
3. Install los sdk que tocan - android update sdk -u -a -t 14
4. Genymotion for Fun https://www.genymotion.com/fun-zone/


## Steps to run:
1. Start Appium properly

2. for Android, you need to Start Genymotion Android Emulator manually

3. Use any Java IDE, e.g. IntelliJ, to import this maven project,

4. After project is imported, open the source code SampleTest.java, right click method firstTest and run

5. or set environment var, e.g. device=android, and then run step3, will start Android test


## Expected Automated Sample Test Result (SampleTest):

1. Start the mobile simulator (for iOS only; for Android, please start Genymotion Android Emulator manually)

2. Open mobile browser, e.g., iOS safari or Android Browser

3. Visit "http://google.com"

4. Verify if the title is "Google"

5. Quit the driver

## Expected Automated Calculator Test Result (CalculatorTest):

1. Open Calculator app

2. Press 1

3. Press +

4. Press 5

5. Expected result 6

## Upload files for SauceLabs
For Android
```
curl -u itnove:4394d787-3244-4c03-9490-3816f2bb683b -X POST -H "Content-Type: application/octet-stream" https://saucelabs.com/rest/v1/storage/itnove/Calculator.apk\?overwrite\=true --data-binary @src/test/resources/Calculator.apk
```

For IOS
```
curl -u itnove:4394d787-3244-4c03-9490-3816f2bb683b -X POST -H "Content-Type: application/octet-stream" https://saucelabs.com/rest/v1/storage/itnove/Calculator.zip\?overwrite\=true --data-binary @src/test/resources/Calculator.zip
```

## Support

This tutorial is released into the public domain by ITNove under WTFPL.

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)](http://www.wtfpl.net/)

This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) and is likewise released into the public domain.

Please contact ITNove for further details.

* ITNOVE a Cynertia Consulting
* Passeig de Gràcia 110, 4rt 2a
* 08008 Barcelona
* T: 93 184 53 44