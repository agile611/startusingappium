package com.itnove.pages.amazon;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by guillem on 01/03/16.
 */
public class AmazonCheckoutPage {

    private WebDriver driver;

    @FindBy(id = "a-autoid-0-announce")
    public WebElement checkoutButton;

    public AmazonCheckoutPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public boolean isCheckoutButtonPresent() {
        return checkoutButton.isDisplayed();
    }
}
