package com.itnove.tests.calculator;


import com.itnove.apps.SauceMobileAppTest;
import com.itnove.utils.WebUtil;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;

/**
 * Created by guillemhs on 2015-12-28.
 */
public class SauceCalculatorDivideTest extends SauceMobileAppTest {
    @Test
    public void divideShouldWork() throws InterruptedException {
        // select the first number
        WebUtil.click(driver, locNumber6.get(deviceNum));
        Thread.sleep(2500);
        // select the operator
        WebUtil.click(driver, locOperDivide.get(deviceNum));
        Thread.sleep(2500);
        // select the second number
        WebUtil.click(driver, locNumber1.get(deviceNum));
        Thread.sleep(2500);
        // click equals to calculate the result
        WebUtil.click(driver, locEquals.get(deviceNum));
        Thread.sleep(2500);
        // verify the result
        WebElement resultElement = WebUtil.waitAndGetVisibleElement(driver, locResult.get(deviceNum));
        Thread.sleep(2500);
        Assert.assertEquals("6 / 1 should be 6", 6, Integer.parseInt(resultElement.getText()));
    }
}
