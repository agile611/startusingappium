package com.itnove.pages.amazon;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by guillem on 01/03/16.
 */
public class AmazonResultsPage {

    private WebDriver driver;

    @FindBy(xpath = "(.//*[@class='sx-table-row'])[1]")
    public WebElement firstItem;

    public AmazonResultsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void clickOnFirstItem() {
        firstItem.click();
    }
}
