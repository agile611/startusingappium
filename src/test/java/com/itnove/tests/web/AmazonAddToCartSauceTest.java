package com.itnove.tests.web;

import com.itnove.browsers.SauceBaseBrowserTest;
import com.itnove.pages.amazon.AmazonHomePage;
import com.itnove.pages.amazon.AmazonProductPage;
import com.itnove.pages.amazon.AmazonResultsPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by guillemhs on 2017-11-28
 */
public class AmazonAddToCartSauceTest extends SauceBaseBrowserTest {
    @Test
    public void searchTest() throws InterruptedException {
        System.out.println(driver.getSessionId());
        driver.navigate().to("http://www.web.com");
        AmazonHomePage homePage = new AmazonHomePage(driver);
        homePage.searchKeyword("tv 4k");
        Thread.sleep(6000);
        AmazonResultsPage resultsPage = new AmazonResultsPage(driver);
        resultsPage.clickOnFirstItem();
        Thread.sleep(6000);
        AmazonProductPage productPage = new AmazonProductPage(driver);
        productPage.clickOnAddToCartButton();
    }
}
