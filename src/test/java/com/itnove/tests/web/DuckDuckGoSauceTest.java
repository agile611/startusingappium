package com.itnove.tests.web;

import com.itnove.browsers.MobileBrowserBaseTest;
import com.itnove.browsers.SauceBaseBrowserTest;
import com.itnove.pages.duckduckgo.ResultsPage;
import com.itnove.pages.duckduckgo.SearchPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by guillemhs on 2017-11-28
 */
public class DuckDuckGoSauceTest extends MobileBrowserBaseTest {
    @Test
    public void searchTest() throws InterruptedException {
        System.out.println(driver.getSessionId());
        driver.navigate().to("http://www.duckduckgo.com");
        SearchPage searchPage = new SearchPage(driver);
        searchPage.searchKeyword("Hawaiian pizza");
        Thread.sleep(2000);
        ResultsPage resultsPage = new ResultsPage(driver);
        assertTrue(resultsPage.isResultsListPresent());
        resultsPage.clickOnFirstResult();
        Thread.sleep(2000);
        assertTrue(!driver.getCurrentUrl().contains("duckduckgo"));
    }
}
