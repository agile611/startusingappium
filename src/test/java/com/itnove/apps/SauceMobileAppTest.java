package com.itnove.apps;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by guillemhs on 2015-11-29.
 */
public class SauceMobileAppTest {
    public RemoteWebDriver driver;
    public final String[] number0 = {"net.tecnotopia.SimpleCalculator:id/button0", "0"};
    public final String[] number1 = {"net.tecnotopia.SimpleCalculator:id/button1", "1"};
    public final String[] number2 = {"net.tecnotopia.SimpleCalculator:id/button2", "2"};
    public final String[] number3 = {"net.tecnotopia.SimpleCalculator:id/button3", "3"};
    public final String[] number4 = {"net.tecnotopia.SimpleCalculator:id/button4", "4"};
    public final String[] number5 = {"net.tecnotopia.SimpleCalculator:id/button5", "5"};
    public final String[] number6 = {"net.tecnotopia.SimpleCalculator:id/button6", "6"};
    public final String[] number7 = {"net.tecnotopia.SimpleCalculator:id/button7", "7"};
    public final String[] number8 = {"net.tecnotopia.SimpleCalculator:id/button8", "8"};
    public final String[] number9 = {"net.tecnotopia.SimpleCalculator:id/button9", "9"};
    public final String[] operAdd = {"net.tecnotopia.SimpleCalculator:id/buttonAdd", "+"};
    public final String[] operMinus = {"net.tecnotopia.SimpleCalculator:id/buttonAdd", "-"};
    public final String[] operMultiply = {"net.tecnotopia.SimpleCalculator:id/buttonAdd", "x"};
    public final String[] operDivide = {"net.tecnotopia.SimpleCalculator:id/buttonAdd", "/"};
    public final String[] equals = {"net.tecnotopia.SimpleCalculator:id/buttonEquals", "="};
    public final String[] result = {"net.tecnotopia.SimpleCalculator:id/display", "6"};

    public final List<By> locNumber0 = new ArrayList<>();
    public final List<By> locNumber1 = new ArrayList<>();
    public final List<By> locNumber2 = new ArrayList<>();
    public final List<By> locNumber3 = new ArrayList<>();
    public final List<By> locNumber4 = new ArrayList<>();
    public final List<By> locNumber5 = new ArrayList<>();
    public final List<By> locNumber6 = new ArrayList<>();
    public final List<By> locNumber7 = new ArrayList<>();
    public final List<By> locNumber8 = new ArrayList<>();
    public final List<By> locNumber9 = new ArrayList<>();
    public final List<By> locOperAdd = new ArrayList<>();
    public final List<By> locOperMinus = new ArrayList<>();
    public final List<By> locOperMultiply = new ArrayList<>();
    public final List<By> locOperDivide = new ArrayList<>();
    public final List<By> locEquals = new ArrayList<>();
    public final List<By> locResult = new ArrayList<>();

    public String device;
    public int deviceNum;

    @Before
    public void setUp() throws MalformedURLException {
        // by default, start android app
        device = "ios";
        deviceNum = 1;
        // try to set according to user requirement
        if (System.getProperty("device") != null) {
            device = System.getProperty("device");
        } else {
            System.out.println("device not defined, will use default = " + device);
        }
        deviceNum = device.equalsIgnoreCase("Android") ? 0 : 1;

        locNumber0.add(By.id(number0[0]));
        locNumber0.add(By.name(number0[1]));
        locNumber1.add(By.id(number1[0]));
        locNumber1.add(By.name(number1[1]));
        locNumber2.add(By.id(number2[0]));
        locNumber2.add(By.name(number2[1]));
        locNumber3.add(By.id(number3[0]));
        locNumber3.add(By.name(number3[1]));
        locNumber4.add(By.id(number4[0]));
        locNumber4.add(By.name(number4[1]));
        locNumber5.add(By.id(number5[0]));
        locNumber5.add(By.name(number5[1]));
        locNumber6.add(By.id(number6[0]));
        locNumber6.add(By.name(number6[1]));
        locNumber7.add(By.id(number7[0]));
        locNumber7.add(By.name(number7[1]));
        locNumber8.add(By.id(number8[0]));
        locNumber8.add(By.name(number8[1]));
        locNumber9.add(By.id(number9[0]));
        locNumber9.add(By.name(number9[1]));


        locOperAdd.add(By.id(operAdd[0]));
        locOperAdd.add(By.name(operAdd[1]));
        locOperMinus.add(By.id(operMinus[0]));
        locOperMinus.add(By.name(operMinus[1]));
        locOperMultiply.add(By.id(operMultiply[0]));
        locOperMultiply.add(By.name(operMultiply[1]));
        locOperDivide.add(By.id(operDivide[0]));
        locOperDivide.add(By.name(operDivide[1]));
        locEquals.add(By.id(equals[0]));
        locEquals.add(By.name(equals[1]));
        locResult.add(By.id(result[0]));
        locResult.add(By.name(result[1]));

        // switch between diffrent browsers, e.g. iOS Safari or Android Chrome
        // let's use the os name to differentiate, because we only use default browser in that os
        if (device != null && device.equalsIgnoreCase("Android")) {
            DesiredCapabilities capabilities = DesiredCapabilities.android();
            capabilities.setCapability("appiumVersion", "1.4.16");
            capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
            capabilities.setCapability("deviceName", "Android Emulator");
            capabilities.setCapability("browserName", "");
            capabilities.setCapability("platformVersion", "4.4");
            capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("app", "sauce-storage:Calculator.apk");
            driver = new AndroidDriver(new URL("https://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:443/wd/hub"), capabilities);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        } else {
            DesiredCapabilities caps = DesiredCapabilities.iphone();
            caps.setCapability("appiumVersion", "1.4.16");
            caps.setCapability("deviceName", "iPhone 6");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("platformVersion", "9.2");
            caps.setCapability("platformName", "iOS");
            caps.setCapability("browserName", "");
            caps.setCapability("app", "sauce-storage:Calculator.zip");
            driver = new IOSDriver(new URL("https://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:443/wd/hub"), caps);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }
        System.out.println(driver.getSessionId());
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
