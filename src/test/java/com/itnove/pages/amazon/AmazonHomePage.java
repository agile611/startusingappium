package com.itnove.pages.amazon;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by guillem on 01/03/16.
 */
public class AmazonHomePage {

    private WebDriver driver;

    @FindBy(id = "nav-search-keywords")
    public WebElement searchBox;

    @FindBy(xpath = "id('nav-search-form')/div[1]/div[1]/input[1]")
    public WebElement searchButton;

    public AmazonHomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void searchKeyword(String keyword) {
        searchBox.click();
        searchBox.sendKeys(keyword);
        searchButton.click();
    }
}
